import React, { Component } from 'react'
import Feature_Box from './Feature_Box'
import Footer from './Footer'
import Free_Download from './Free_Download'
import Fresh_Layout from './Fresh_Layout'
import Jumbotron from './Jumbotron'
import Just_Name from './Just_Name'
import Nav from './Nav'
import Simple_Code from './Simple_Code'
import Welcome from './Welcome'

export default class React_Buoi1 extends Component {
    render() {
        return (
            <div>
                <Nav />
                <Welcome />
                <div className='pt-4'>
                    <div className="container px-lg-5">
                        <div className="row gx-lg-5">
                            <Fresh_Layout />
                            <Free_Download />
                            <Jumbotron />
                            <Feature_Box />
                            <Simple_Code />
                            <Just_Name />
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}
